// Erathostene.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
enum Boolean
{
	False,
	True
};
struct Number
{
	int m_Num;
	Number* m_Before;
	Number* m_Ater;
};
Number* New_Chainon(Number *Ptr_Before)
{
	Number* New_Number = (Number*)malloc(sizeof(Number));
	New_Number->m_Before = Ptr_Before;
	New_Number->m_Ater = nullptr;
	New_Number->m_Num = 0;
	return New_Number;
}
void Init_List(int p_Size_Of_List,Number* p_number)
{
	Number* Ptr = p_number;
	if (p_Size_Of_List < 2)
	{
		printf("\n Aucun interet de remplir une liste de nombre premier avec seulement des valeurs qui ne seront pas premiere");
	}
	Ptr->m_Num = 1;
	for (int i = 2; i < p_Size_Of_List; i++)
	{
		Number* New_number = New_Chainon(Ptr);
		Ptr->m_Ater = New_number;
		New_number->m_Num = i;
		New_number->m_Before = Ptr;
		New_number->m_Ater = nullptr;
		Ptr = New_number;
		
	}
}
void Suppr_Value(Number* Ptr_Value)
{
	Number* Pivot = Ptr_Value;
	if (Pivot->m_Before != nullptr)
	{
		Pivot->m_Before->m_Ater = Pivot->m_Ater;
		if(Pivot->m_Ater != nullptr)
			Pivot->m_Ater->m_Before = Pivot->m_Before;
		Pivot = Pivot->m_Before;
	}
	else if (Pivot->m_Ater != nullptr) // if first value
	{
		Pivot = Pivot->m_Ater;
		Pivot->m_Before = nullptr;
	}
	free(Ptr_Value);
	*Ptr_Value = *Pivot;
	
}
int Get_Last_Value(Number* P_number)
{
	while (P_number->m_Ater != nullptr)
	{
		P_number = P_number->m_Ater;
	}
	return P_number->m_Num;
}
void Delete_all_Modulo(Number* p_number)
{
	int Modulo = p_number->m_Num;
	
	if (p_number->m_Ater != nullptr)
	{
		Number* Pivot = p_number->m_Ater;
		while (Pivot->m_Ater != nullptr)
		{
			if (Pivot->m_Num % Modulo == 0)
			{
				Suppr_Value(Pivot);
			}
			Pivot = Pivot->m_Ater;
		}
		if (Pivot->m_Num % Modulo == 0)
		{
			Suppr_Value(Pivot);
		}
	}
}
void Erathostene(Number* P_number)
{
	if(P_number->m_Num == 1)
		Suppr_Value(P_number);

	int SQRT = sqrt(Get_Last_Value(P_number));
	while (P_number->m_Num < SQRT)
	{
		Delete_all_Modulo(P_number);
		P_number = P_number->m_Ater;
	}
}
void Print_Tab(Number *p_Number)
{
	while (p_Number->m_Ater != nullptr)
	{
		printf("\n %d", p_Number->m_Num);
		p_Number = p_Number->m_Ater;
	}
}
int main()
{
	int choix = 0;
	Number* Num = (Number*)malloc(sizeof(Number));
	Num->m_Ater = nullptr;
	Num->m_Before = nullptr;
	printf("\nEnter the number of number in the list of prime number (1 and 2 won't be in the list they are not prime");
	scanf_s("%d", &choix);
	Init_List(choix, Num);
	Erathostene(Num);
	Print_Tab(Num);
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu
// nombre premier divisible par lui meme et 1
// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
